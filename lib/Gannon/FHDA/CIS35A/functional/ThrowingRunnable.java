package Gannon.FHDA.CIS35A.functional;

public interface ThrowingRunnable {
   void run() throws Throwable;
}
